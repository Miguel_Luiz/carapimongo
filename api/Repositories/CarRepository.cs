using api.Services;
using api.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace api.Repositories;

public class CarRepository : ICarRepository
{
    private readonly DatabaseContext _context;

    public CarRepository(DatabaseContext context) 
    {
        _context = context;
    }

    public async Task<List<CarEntity>> GetAsync() 
    {
        return await _context.CarsCollection
            .Find(_ => true).ToListAsync();
    }

    public async Task<CarEntity?> GetAsync(string id)
    {
        return await _context.CarsCollection
            .Find(x => x.Id == id).FirstOrDefaultAsync();
    }

    public async Task<CarEntity> CreateAsync(CarEntity entity) 
    {
        await _context.CarsCollection.InsertOneAsync(entity);
        
        return entity;
    }

    public async Task UpdateAsync(CarEntity entity)
    {        
        await _context.CarsCollection
            .ReplaceOneAsync(x => x.Id == entity.Id, entity);
    }

    public async Task RemoveAsync(string id)
    {
        await _context.CarsCollection
            .DeleteOneAsync(x => x.Id == id);
    }
}
  
  