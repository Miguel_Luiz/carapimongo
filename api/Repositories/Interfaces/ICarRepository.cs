using api.Entities;

namespace api.Repositories;

public interface ICarRepository
{
    Task<List<CarEntity>> GetAsync();

    Task<CarEntity?> GetAsync(string id);

    Task<CarEntity> CreateAsync(CarEntity entity);
    
    Task UpdateAsync(CarEntity entity);

    Task RemoveAsync(string id);
  }
  
  