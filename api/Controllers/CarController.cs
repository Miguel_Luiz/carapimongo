using api.Entities;
using api.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharpCompress.Common;

namespace api.Controllers;

// Fazer uma thread para monitorar banco de eventos
// Quando receber um registro, chamar o post ex:

[ApiController]
[Route("api/cars")]
public class CarController : MainController
{
    private readonly ICarRepository _carRepository;

    private readonly IMapper _mapper;

    public CarController(ICarRepository carRepository, IMapper mapper) 
    {
        _carRepository = carRepository;

        _mapper = mapper;
    }

    [HttpGet(Name = "CarControllerGetAll")]
    public async Task<ActionResult<IEnumerable<object>>> Get()
    {
        List<CarEntity>? entities = await _carRepository.GetAsync();

        if (entities.Count == 0) { return NoContent(); }

        return Ok(
            entities
            .Select(b => MapEntity(b))
            .ToList()
        );
    }

    [HttpGet("{id}", Name = "CarControllerGetById")]
    public async Task<ActionResult<object>> Get(string id)
    {
        CarEntity? entity = await _carRepository.GetAsync(id);

        if (entity == null) {
            return NotFound();
        }

        return Ok(
            MapEntity(entity)
        );
    }

    [HttpPost]
    public async Task<ActionResult> Post(CarEntity request) 
    {
        CarEntity? response = await _carRepository
            .CreateAsync(
                _mapper.Map<CarEntity>(request)
            );
        // Considerar talvez não tenha retorno
        return CreatedAtRoute(
            "CarControllerGetById",
            new {id = response.Id},
            MapEntity(response)
        );
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(
        string id,
        CarEntity request) 
    {
        if (request.Id != id) {
            return BadRequest(
                "ID divergente com a rota informada."
            );
        }

        await _carRepository.UpdateAsync(
            _mapper.Map<CarEntity>(request)
        );
        
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        await _carRepository.RemoveAsync(id);

        return NoContent();
    }
}