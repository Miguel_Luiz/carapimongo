using api.Entities;
using AvalNoSql.Services.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace api.Services;

public class DatabaseContext 
{
    public IMongoCollection<BrandEntity> BrandsCollection {get; set;}

    public IMongoCollection<ModelEntity> ModelsCollection {get; set;}

    public IMongoCollection<CarEntity> CarsCollection {get; set;}


    public DatabaseContext(IConfiguration configuration) 
    {
        // string? connectionString = Environment.GetEnvironmentVariable("MONGODB_URI");

        // if (connectionString == null)
        // {
        //     //export MONGODB_URI='mongodb+srv://pabloMarques:4rkP1ZZiPKaxzgCf@banco.rhyk4yh.mongodb.net/?retryWrites=true&w=majority'
        //     Console.WriteLine("You must set your 'MONGODB_URI' environment variable.");
        //     Environment.Exit(0);
        // }

        MongoClient client = new MongoClient(
            // connectionString
            configuration.GetConnectionString("MongoDbConnection")
        );

        IMongoDatabase database = client.GetDatabase("Banco");

        BrandsCollection = database.GetCollection<BrandEntity>("brands");

        ModelsCollection = database.GetCollection<ModelEntity>("models");

        CarConfiguration.Configure();

        CarsCollection = database.GetCollection<CarEntity>("cars");
    }
}