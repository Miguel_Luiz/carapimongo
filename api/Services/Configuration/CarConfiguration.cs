﻿using api.Entities;
using MongoDB.Bson.Serialization;


namespace AvalNoSql.Services.Configuration
{
    public class CarConfiguration : BsonClassMap<CarEntity>
    {
        public static void Configure()
        {
            if (!IsClassMapRegistered(typeof(CarEntity)))
            {
                RegisterClassMap<CarEntity>(cm =>
                {
                    cm.AutoMap();

                    cm.SetIgnoreExtraElements(true); // Se não encontrar campo no documento que match com a entidade, não retorna exceção

                    cm.SetIdMember(cm.GetMemberMap(c => c.Id));

                    cm.MapMember(c => c.Id)
                        .SetElementName("Id")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.ModelId)
                        .SetElementName("ModelId")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.Name)
                        .SetElementName("Name")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.Renavam)
                        .SetElementName("Renavam")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.Plate)
                        .SetElementName("Plate")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.Value)
                        .SetElementName("Value")
                        .SetIgnoreIfDefault(true);

                    cm.MapMember(c => c.Year)
                        .SetElementName("Year")
                        .SetIgnoreIfDefault(true);
                });
            }
        }
    }
}
